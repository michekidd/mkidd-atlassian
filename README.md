# Michelle Kidd UI-exercise

Thank you for viewing my coding exercise.

I want to note a few things before you review.

I used osprey-mock-server to use the raml file as a mock api. Because my api requests only returned sample data that did not match parameters, I was not able to handle network/Api failures as eloquently as I would have liked to. Also, I handled the returned data in a few hacky ways due to not being able to make requests with parameters.

## Installing

Navigate to mkidd-atlassian in your terminal.

Run the following:

npm install

npm install -g osprey-mock-service

osprey-mock-service -f space-api.raml -p 3001 --cors

npm start

Navigate to http://localhost:3000/ in your browser


