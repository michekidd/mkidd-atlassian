import React, { Component } from "react";
import { BrowserRouter, Redirect } from "react-router-dom";
import Nav from "./nav/nav";
import Title from "./Title";
import SpaceData from "./spaceData";
import "./App.css";

class App extends Component {
  state = {
    activeSpaceId: "yadj1kx9rmg0",
    spaces: [],
    redirected: false,
    spacesError: false
  };

  componentDidMount() {
    this.getSpacesData();
    this.setState({
      redirected: true
    });
  }

  componentDidUpdate(prevProps, prevState) {
    const { redirected } = this.state;

    if (redirected && !prevState.redirected) {
      this.setState({
        redirected: false
      });
    }
  }
  // The next 2 methods would be handled differently had I been able to make an API call with params.
  //Instead I'm using the last number of the activeSpace Id.
  getTitle = () => {
    const { spaces, activeSpaceId } = this.state;
    const arrayPosition = activeSpaceId[activeSpaceId.length - 1];
    return spaces[0] ? spaces[arrayPosition].fields.title : null;
  };

  getAuthor = () => {
    const { spaces, activeSpaceId } = this.state;
    const arrayPosition = activeSpaceId[activeSpaceId.length - 1];
    return spaces[0] ? spaces[arrayPosition].fields.description : null;
  };

  getSpacesData = async () => {
    const url = "http://localhost:3001/space/";
    try {
      const response = await fetch(url);
      const json = await response.json();
      response.ok &&
        this.setState({
          spaces: json.items
        });
    } catch (e) {
      this.setState({ spacesError: true });
    }
  };

  changeActiveSpace = spaceId => {
    const { activeSpaceId } = this.state;
    if (activeSpaceId !== spaceId) {
      this.setState({
        activeSpaceId: spaceId
      });
    }
  };

  render() {
    const { activeSpaceId, spaces, redirected, spacesError } = this.state;
    const notFirstSpace = activeSpaceId !== "yadj1kx9rmg0";
    return (
      <BrowserRouter>
        {redirected && <Redirect to={`/${activeSpaceId}`} />}
        <div className="bodyWrapper" data-test="component-app">
          <Nav
            spaces={spaces}
            activeSpaceId={activeSpaceId}
            changeSpace={this.changeActiveSpace}
            spacesError={spacesError}
          />
          <div className="body">
            <Title title={this.getTitle()} error={spacesError} />
            <SpaceData
              activeSpaceId={activeSpaceId}
              notFirstSpace={notFirstSpace}
            />
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
