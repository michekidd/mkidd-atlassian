import React from "react";
import PropTypes from "prop-types";

const Title = ({ spacesError, title }) => {
  return (
    <h1 data-test="title">
      {!spacesError ? title : "We are having issues accessing this data"}
    </h1>
  );
};

Title.propTypes = {
  spacesError: PropTypes.bool,
  title: PropTypes.string
};

export default Title;
