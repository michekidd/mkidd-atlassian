import React from "react";
import Title from "./Title";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
//configure adapter
Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<Title {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

test("renders text within title", () => {
  const wrapper = setup({ spacesError: false, title: "Testing Title" });
  const titleComponent = findByTestAttr(wrapper, "title");
  expect(titleComponent.text().length).not.toBe(0);
});
