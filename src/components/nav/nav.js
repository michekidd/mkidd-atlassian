import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import "./nav.css";

const Nav = ({ activeSpaceId, changeSpace, spaces, spacesError }) => {
  return (
    <nav data-test="nav-component">
      <ul className="ul">
        {!spacesError &&
          spaces.map(space => (
            <li
              key={space.sys.id}
              className={
                "spaceButton " +
                (activeSpaceId === space.sys.id ? "active" : null)
              }
            >
              <button
                type="button"
                onClick={() => {
                  changeSpace(space.sys.id);
                }}
              >
                <Link to={`/${space.sys.id}`}>{space.fields.title}</Link>
              </button>
            </li>
          ))}
        {spacesError && (
          <div className="spaceButton" data-test="nav-data-error">
            I'm sorry. We couldn't access your spaces.
          </div>
        )}
      </ul>
    </nav>
  );
};

Nav.propTypes = {
  spaces: PropTypes.arrayOf(
    PropTypes.shape({
      fields: PropTypes.shape({
        description: PropTypes.string,
        title: PropTypes.string
      }),
      sys: PropTypes.shape({
        createdAt: PropTypes.string,
        createdBy: PropTypes.string,
        id: PropTypes.string,
        type: PropTypes.string,
        updatedAt: PropTypes.string,
        updatedBy: PropTypes.string
      })
    })
  ),
  activeSpaceId: PropTypes.string.isRequired,
  changeSpace: PropTypes.func.isRequired,
  spacesError: PropTypes.bool
};

export default Nav;
