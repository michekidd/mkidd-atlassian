import React from "react";
import Nav from "./Nav";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
//configure adapter
Enzyme.configure({ adapter: new EnzymeAdapter() });

// const setup = (props = {}, state = null) => {
//   const wrapper = shallow(<Nav {...props} />);
//   if (state) wrapper.setState(state);
//   return wrapper;
// };

const defaultProps = {
  spaces: [
    {
      fields: {
        description: "test",
        title: "test"
      },
      sys: {
        createdAt: "test",
        createdBy: "test",
        id: "test",
        type: "test",
        updatedAt: "test",
        updatedBy: "test"
      }
    }
  ],
  activeSpaceId: "testId",
  changeSpace: () => {},
  spacesError: false
};

const setup = (props = {}) => {
  const setupProps = { ...defaultProps, ...props };
  return shallow(<Nav {...setupProps} />);
};

const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
};

test("renders without error", () => {
  const wrapper = setup();
  const appComponent = findByTestAttr(wrapper, "nav-component");
  expect(appComponent.length).toBe(1);
});

test("show error when spacesError is True", () => {
  const wrapper = setup({
    spacesError: true
  });
  const appComponent = findByTestAttr(wrapper, "nav-data-error");
  expect(appComponent.length).toBe(1);
});
