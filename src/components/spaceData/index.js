import React, { Fragment, Component } from "react";
import Row from "./row/row";
import sort from "../../assets/sort.svg";
import base64 from "base-64";
import "./spaceData.css";

class SpaceData extends Component {
  state = {
    rowData: [],
    activeTab: "Entries",
    body: "<h4>Please select an entry.</h4>",
    authors: {},
    dataError: false
  };

  componentDidMount() {
    const { notFirstSpace } = this.props;
    if (notFirstSpace) {
      this.setState({ dataError: true });
    }
    this.getTableData();
    this.getAuthors();
  }

  componentDidUpdate(prevProps, prevState) {
    const { activeTab } = this.state;
    const { notFirstSpace } = this.props;
    if (activeTab !== prevState.activeTab) {
      this.getTableData();
    }
    if (notFirstSpace !== prevProps.notFirstSpace) {
      if (notFirstSpace === true) {
        this.setState({ dataError: true });
      } else {
        this.setState({ dataError: false });
      }
    }
  }

  getAuthors = async userId => {
    const url = "http://localhost:3001/users";
    const response = await fetch(url);
    const json = await response.json();
    let authors = {};

    json.items.forEach(author => {
      authors[author.sys.id] = author.fields.name;
    });
    this.setState({ authors: authors });
  };

  getTableData = async () => {
    const { activeSpaceId } = this.props;
    const { activeTab } = this.state;
    const entryOrAsset = activeTab === "Entries" ? "entries" : "assets";
    const url = `http://localhost:3001/space/${activeSpaceId}/${entryOrAsset}`;
    try {
      const response = await fetch(url);
      const json = await response.json();
      const data = [];

      if (activeTab === "Entries") {
        json.items.forEach(entry => {
          data.push({
            id: entry.sys.id,
            title: entry.fields.title,
            summary: entry.fields.summary,
            createdBy: entry.sys.createdBy,
            updatedBy: entry.sys.updatedBy,
            updatedAt: entry.sys.updatedAt,
            body: entry.fields.body
          });
        });
        //adding some additional data to confirm sorting is working
        data.push(
          {
            id: 23,
            title: "More Data",
            summary: "I need more data to test my sorting",
            createdBy: "Michelle Kidd",
            updatedBy: "Michelle Kidd",
            updatedAt: "12-01-2019",
            body: base64.encode("<p>This is some text.</p>")
          },
          {
            id: 24,
            title: "Even More Data",
            summary: "I need more even more data to test my sorting",
            createdBy: "Michelle Kidd",
            updatedBy: "Michelle Kidd",
            updatedAt: "1-01-2019",
            body: base64.encode("<p>This is some more text.</p>")
          }
        );
      } else {
        json.items.forEach(entry => {
          data.push({
            id: entry.sys.id,
            title: entry.fields.title,
            contentType: entry.fields.contentType,
            fileName: entry.fields.fileName,
            createdBy: entry.sys.createdBy,
            updatedBy: entry.sys.updatedBy,
            updatedAt: entry.sys.updatedAt,
            src: entry.fields.upload
          });
        });
      }
      return response.ok
        ? this.setState({ rowData: data })
        : Promise.reject(json);
    } catch (e) {
      this.setState({ dataError: true });
    }
  };

  compareBy = key => {
    return (a, b) => {
      if (key === "updatedAt") {
        return new Date(b[key]) - new Date(a[key]);
      } else {
        if (a[key] < b[key]) return -1;
        if (a[key] > b[key]) return 1;
        return 0;
      }
    };
  };

  sortBy = key => {
    const { rowData } = this.state;
    const arrayCopy = [...rowData];
    arrayCopy.sort(this.compareBy(key));
    this.setState({ rowData: arrayCopy });
  };

  setActiveTab = tab => {
    const { activeTab } = this.state;
    if (tab !== activeTab) {
      this.setState({ activeTab: tab });
    }
  };

  selectEntry = (entry, author) => {
    this.setState({ body: entry, author: author });
  };

  render() {
    const { rowData, activeTab, body, authors, author, dataError } = this.state;
    const rows = rowData.map(rowFields => (
      <Row
        key={rowFields.id}
        {...rowFields}
        authors={authors}
        selectEntry={this.selectEntry}
        activeTab={activeTab}
      />
    ));

    return (
      <Fragment>
        <h4> {author ? `Created by ${author}` : null}</h4>
        <div className="summary" dangerouslySetInnerHTML={{ __html: body }} />
        <div className="tableWrapper">
          <div>
            <div
              className={
                "tab " + (activeTab === "Entries" ? "activeTab" : null)
              }
              onClick={() => this.setActiveTab("Entries")}
            >
              Entries
            </div>
            <div
              className={"tab " + (activeTab === "Assets" ? "activeTab" : null)}
              onClick={() => this.setActiveTab("Assets")}
            >
              Assets
            </div>
          </div>
          <table>
            <tbody>
              <tr className="tableHeadRow">
                <th onClick={() => this.sortBy("title")}>
                  Title <img className="sort" src={sort} alt="sort" />
                </th>
                <th
                  onClick={() =>
                    this.sortBy(
                      activeTab === "Entries" ? "summary" : "contentType"
                    )
                  }
                >
                  {activeTab === "Entries" ? "Summary" : "Content Type"}
                  <img className="sort" src={sort} alt="sort" />
                </th>
                {activeTab === "Assets" && (
                  <th onClick={() => this.sortBy("fileName")}>
                    File Name <img className="sort" src={sort} alt="sort" />
                  </th>
                )}
                <th onClick={() => this.sortBy("createdBy")}>
                  Created By <img className="sort" src={sort} alt="sort" />
                </th>
                <th onClick={() => this.sortBy("updatedBy")}>
                  Updated By <img className="sort" src={sort} alt="sort" />
                </th>
                <th onClick={() => this.sortBy("updatedAt")}>
                  Last Updated <img className="sort" src={sort} alt="sort" />
                </th>
              </tr>
            </tbody>
            <tbody>
              {!dataError ? (
                rows
              ) : (
                <tr>
                  <td>We had issues accessing this data</td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </Fragment>
    );
  }
}

export default SpaceData;
