import React from "react";
import base64 from "base-64";
import Moment from "react-moment";

const Row = props => {
  const {
    id,
    title,
    summary,
    updatedAt,
    body,
    authors,
    createdBy,
    updatedBy,
    activeTab,
    contentType,
    fileName,
    src
  } = props;

  const entriesActive = activeTab === "Entries";
  return (
    <tr
      key={id}
      onClick={() =>
        this.props.selectEntry(
          entriesActive
            ? base64.decode(body)
            : `<img class='assetImage' src=${src} alt='asset' />`,
          authors[createdBy] ? authors[createdBy] : "Michelle Kidd"
        )
      }
    >
      <td>{title} </td>
      <td>{entriesActive ? summary : contentType} </td>
      {!entriesActive ? <td>{fileName}</td> : null}
      <td>{authors[createdBy] ? authors[createdBy] : "Michelle Kidd"}</td>
      <td>{authors[updatedBy] ? authors[updatedBy] : "Michelle Kidd"}</td>
      <td>
        <Moment format="MM/DD/YYYY">{updatedAt}</Moment>
      </td>
    </tr>
  );
};

export default Row;
